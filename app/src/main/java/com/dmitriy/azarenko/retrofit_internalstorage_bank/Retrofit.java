package com.dmitriy.azarenko.retrofit_internalstorage_bank;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by Дмитрий on 19.02.2016.
 */
public class Retrofit {



    private static final String ENDPOINT = "https://api.privatbank.ua/p24api";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/pboffice?json&city=&address=")
        void getDepartment(Callback<List<Department>> callback);


    }

    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getDepartment(Callback<List<Department>> callback) {
        apiInterface.getDepartment(callback);
    }

}
