package com.dmitriy.azarenko.retrofit_internalstorage_bank;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {



    TextView name;
    TextView city;
    TextView adress;
    TextView phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.department_name_id);
        city = (TextView) findViewById(R.id.city_id);
        adress = (TextView) findViewById(R.id.adress_id);
        phone = (TextView) findViewById(R.id.phone_id);


        final ListView listView = (ListView)findViewById(R.id.listView);

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                // прочитал файл "name.dep" из Download
                FileInputStream fIS =
                        new FileInputStream(
                                new File(Environment.getExternalStoragePublicDirectory(
                                        Environment.DIRECTORY_DOWNLOADS), "name.dep")
                        );
                ObjectInputStream oIS = new ObjectInputStream(fIS);
                ArrayList<Department> dep = (ArrayList<Department>) oIS.readObject();
                oIS.close();
                listView.setAdapter(new MyAdapter(this,dep));
                Log.v("MyApp","File has been readed");
            } catch(Exception ex) {
                ex.printStackTrace();
                Log.v("MyApp","File didn't readed");
            }
        }




        Retrofit.getDepartment(new Callback<List<Department>>() {
            @Override
            public void success(List<Department> departments, Response response) {

                if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                    try {
                        // записал файл "name.dep" в Download
                        FileOutputStream fos =
                                new FileOutputStream(
                                        new File(Environment.getExternalStoragePublicDirectory(
                                                Environment.DIRECTORY_DOWNLOADS), "name.dep")
                                );
                        ObjectOutputStream os = new ObjectOutputStream(fos);
                        os.writeObject(departments);
                        os.close();
                        Log.v("MyApp","File has been written");
                    } catch(Exception ex) {
                        ex.printStackTrace();
                        Log.v("MyApp", "File didn't write");
                    }
                }

                listView.setAdapter(new MyAdapter(MainActivity.this, departments));


            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(),"Something wrong",Toast.LENGTH_SHORT).show();

            }
        });


    }


    class MyAdapter extends ArrayAdapter<Department> {

        public MyAdapter(Context context, List<Department> objects) {
            super(context, R.layout.list_item, objects);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.textNameofDepartment = (TextView) rowView.findViewById(R.id.department_name_id);
                holder.textCity = (TextView) rowView.findViewById(R.id.city_id);
                holder.textAdress = (TextView) rowView.findViewById(R.id.adress_id);
                holder.textPhone = (TextView) rowView.findViewById(R.id.phone_id);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }

            Department department = getItem(position);
            holder.textNameofDepartment.setText(department.getName());
            holder.textCity.setText(department.getCity());
            holder.textAdress.setText(department.getAddress());
            holder.textPhone.setText(department.getPhone());


            return rowView;
        }

        class ViewHolder {

            public TextView textNameofDepartment;
            public TextView textCity;
            public TextView textAdress;
            public TextView textPhone;
        }



    }

}
